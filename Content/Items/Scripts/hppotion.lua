
function Init()
    effect = tonumber(packetInfo:Get("effect"))
end

function OnUse(monster)
    monster:SetHP(math.min(monster:GetHP()+effect, monster:GetMaxHP()))
end